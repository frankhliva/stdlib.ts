import { LazyExpr } from './LazyExpr';
import { Option, TOption } from './Option';
import { Json } from './Json';

export class ErrorReader {
    protected readonly lazyMessage = new LazyExpr(() => this.tryParseMessage(this.error));

	protected tryParseMessage(error: any) {
		if (error.message && error.message.toString) {
			const message = error.message.toString();
			return Option.some(message);
		} else {
			return Option.none();
		}
	}

	protected tryParseJson(message: string) {
		return message.substring(message.indexOf('{') - 1);
	}

    protected readonly lazyJSON = new LazyExpr(() => {
        const messageOption = this.lazyMessage.value;
        
		return (
			messageOption.kind === "Some"
				? Option.some(this.tryParseJson(messageOption.value))
				: Option.none()
		);
    });

	protected readonly lazyData = new LazyExpr(() => {
		const jsonOption = this.lazyJSON.value;
		return (
			jsonOption.kind === "Some"
				? Json.tryParse(jsonOption.value)
				: Option.none()
		);
	});

	constructor(protected readonly error) {}

	tryGetMessage() {
		return this.lazyMessage.value;
	}

	tryGetJson() {
		return this.lazyJSON.value;
	}

	tryGetData<T = any>() {
		return this.lazyData.value as TOption<T>;
	}
}