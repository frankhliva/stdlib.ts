import * as stdio from 'extsprintf';

export type TStringTemplate<T> = ((value: T) => string) | string;

export module StringTemplate {
        
    export function toString<T>(template: TStringTemplate<T>, value: T) {
        return (
            typeof template === 'string'
                    ? stdio.sprintf(template, value)
                    : template(value)
        )
    }

    export function toStringIfExistsValue<T>(template: TStringTemplate<T>, value: T | null | undefined, defaultValue: string = '') {
        return (
            value === null || value === undefined
                    ? defaultValue
                    : toString(template, value)
        );
    }
}